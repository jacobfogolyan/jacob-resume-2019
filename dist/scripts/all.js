$(document).ready(function() {

    /* Every time the window is scrolled ... */
    $(window).scroll( function(){

        /* Check the location of each desired element */
        $('.hideme').each( function(i){

            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){

                $(this).animate({'opacity':'1'},500);

            }
        });
    });
});
var sticky = (function(sticky) {
	const object = {}
	console.log('Log message');
	object.init = function (sticky) {
		if (sticky) {
			object.getPosition(sticky)
		} else {
			console.log('nope');
			return false
		}
	}
	//get position in browser

	object.getPosition = function (sticky) {
        var offset = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
        console.log(offset)
        return this
	}
	//detect if scrolling
	//if scroll point pass positoin of sticky
	//stick div to the top
	return object.init(sticky)
});

var runSticky = new sticky(1);
'use strict';

var carousel = (function (param1, param2, dissolve)  {
		const slide = document.querySelectorAll('.slide')
		const switcherContainer = document.querySelector('.switcher-container')
		const switcherRoot = document.querySelector('.switcher')
		const object = {}

		//selecting dynamically generated elements. only possible because inside iffie
		const pagination = document.querySelectorAll('.pagination');
		const button = document.querySelectorAll('.button-selector');

	object.init = function () {
		object.addChildTo('pagination' , switcherRoot)
		//defining pagination needed here for pagination creation process. returning this function will allow pagination querySelector to be globally accessible and
		let pagi = document.querySelector('.pagination')
		object.switcherContainerDimensions()

		object.listener(switcherRoot)
		for (let i = 0; i < slide.length; i ++) {
			object.addChildTo('button-selector' , pagi)
			//sets overall width based on child elements
			if(slide[i].classList.contains('active')) {
				document.querySelectorAll('.button-selector')[i].classList.add('active')
			}
			//adding to array var arr = [ 'A', 'B', 'D', 'E' ]; arr.insert(2, 'C');
		}
	}

	object.addChildTo = function(arrayOfElements, parent) {
	    let creatElementType = document.createElement('div')
	    parent.appendChild(creatElementType).className = arrayOfElements
	    let createdElement = [].slice.call(document.querySelectorAll("." + arrayOfElements))
	    return [arrayOfElements, createdElement]
	}

	object.listener = function(element) {
	    element.addEventListener("click", function(e) {
			let target = event.target
			object.paginationSelector(event, element)
	    })
	    return true
	}
	object.switcherContainerDimensions = function() {
		let widthInterval = 1000
		let totalWidth = 0
		var stored = false

		for (var i = slide.length - 1; i >= 0; i--) {
			totalWidth = i * widthInterval
			totalWidth = totalWidth + 1000
			switcherContainer.style.width += totalWidth + "px"
			return false
		}
		return false
	}
	object.autoSlide = function (dissolve) {

	}
	object.paginationSelector = function (event, element) {
		//this obhect is pass only when a
		 // button-selector is event is clicked
		var hasChanged = true
		var counter = 0
		const button = document.querySelectorAll('.button-selector');
		//this section could be revisited
		for (var i = slide.length - 1; i >= 0; i--) {
			if (event.target.classList.contains('button-selector')) {
				button[i].classList.remove('active')
				slide[i].classList.remove('active')
			}
		}
		event.target.classList.add('active')
		for (var i = slide.length - 1; i >= 0; i--) {
			if(button[i].isEqualNode(event.target) && button[i].classList.contains('active') && event.target.classList.contains('button-selector')) {
				slide[i].classList.add('active')
				let widthInterval = 1000
				let offsetAmount = i * widthInterval
				switcherContainer.style.marginLeft = "-" + offsetAmount + "px"
			}
		}
		return true
	}
	return object.init()
});
var slider = new carousel('param1', 'param2');

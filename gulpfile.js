let gulp = require('gulp');
let sass = require('gulp-sass');
let browserSync = require('browser-sync');
let concat = require('gulp-concat');

gulp.task('browser-sync', () => {
    browserSync.init(["./scss/**/*.scss", "scripts/*.js", '*.html'], {
        server: {
            baseDir: "./dist/"
        }
    });
});

gulp.task('sass', () => {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
    browserSync.reload();
});

gulp.task('html', () => {
	return gulp.src('*.html')
    .pipe(gulp.dest('./dist/'));
	browserSync.reload();
});

gulp.task('scripts', () => {
  return gulp.src('./scripts/*.js')
    .pipe(concat('./all.js'))
    .pipe(gulp.dest('./dist/scripts/'));
    browserSync.reload();
});

gulp.task('default', ['sass', 'browser-sync', 'html', 'scripts'], () => {
    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch("scripts/*.js", ['scripts']);
    gulp.watch("*.html", ['html']);
    browserSync.reload();
});